# Jerome Ching's Portfolio

Dino Rush:
Dinosaur-themed tower defense/offense game, designed and developed in CTIN482 -
Designing Social Games at USC, as a group project. Created in Unity.
Disclaimer: Vertical slice, functionality incomplete.
Group Members:
Christie Xu, Tristan Postley, Keanu Concepcion, Katie Moses, Brian Handy.

ITP380:
Collection of mini-projects and labwork building into a final product,
'Parkour's Edge', found in the Lab12 folder. Developed in ITP380 - Video
Game Programming at USC. Created in Xcode using C++.